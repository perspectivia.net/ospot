<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    version="3.0">
    <xsl:output indent="yes" method="xml"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    
    <xsl:preserve-space elements="*"/>
    <xsl:strip-space elements="tei:tagsDecl"/>
    
    <!-- remove unnecessary language information -->
    <xsl:template match="@xml:lang"/>
    
    <!-- remove unnecessary references to rendition information in the beginning -->
    <!--<xsl:template match="@rendition"/>-->
    
    <!-- remove invisible part attribute -->
    <xsl:template match="@part"/>
    
    <!-- remove rendition information in TEI-Header <tagsDecl> -->
    <!-- <xsl:template match="tei:rendition"/> -->
    
    <!-- contains only information about brackets -->
    <!--<xsl:template match="tei:mentioned/@rend"/>-->
    
    <!-- clean @rend attributes -->
    <xsl:template match="@rend">
        <!-- value of rend tokenized at ; -->
        <xsl:variable name="rend" select="tokenize(.,';')"/>
        <!-- sequence of values we want to delete -->
        <xsl:variable name="toDelete" select="('font-family:Arabic Typesetting', 'font-family:Times New Roman', 'font-family:Calibri',
            'color:#000000', 'color:#400040', 'color:#ff0000', 'color:#00ffff', 'visible bracket', 'visible bracket manualFormat', 'visible showAll bracket', 
            'font-size:10pt', 'font-size:11pt', 'font-size:16pt', 'font-size:20pt', 'font-size:22pt', 'font-size:24pt', 'font-size:26pt', 'font-size:30pt', 'font-size:3pt', 
            'line-height:0.1pt', 'line-height:0.5pt', 'line-height:10pt', 'line-height:14pt', 'line-height:17pt', 'line-height:18pt', 'line-height:20pt', 
            'line-height:24pt', 'line-height:28pt', '-cte-line-height:fixed', '-cte-text-align:justify-center', 'display:none', 'letter-spacing:-0.3pt', 'letter-spacing:-0.6pt', 
            'letter-spacing:-0.7pt', 'letter-spacing:-0.8pt', 'letter-spacing:-0.9pt', 'letter-spacing:-1pt', 'letter-spacing:-2pt', 'letter-spacing:-3pt'
            , 'letter-spacing:-4.4pt', 'letter-spacing:-4pt', 'margin-bottom:10pt', 'margin-bottom:30pt', 'margin-bottom:14pt', 'margin-bottom:15pt', 'margin-bottom:18pt'
            , 'margin-bottom:1pt', 'margin-bottom:20pt', 'margin-bottom:4pt', 'margin-bottom:5pt', 'margin-bottom:7pt', 'margin-left:12.5mm',
            'margin-top:10pt', 'margin-top:15pt', 'margin-top:1pt', 'margin-top:20pt', 'margin-top:4pt', 'margin-top:8pt', 
            'tab-stops:center blank 80mm', 'tab-stops:left blank 12.5mm left blank 25mm left blank 37.5mm left blank 50mm left blank 62.4mm left blank 70.2mm', 
            'tab-stops:left blank 56.4mm', 'tab-stops:left blank 62.2mm center blank 80mm', 'tab-stops:left blank 68.1mm center blank 80mm', 
            'text-align:justify', 'text-align:left', 'text-indent:12.5mm')"/>
        
        <!-- build index of values to kick out -->
        <xsl:variable name="kickIndex" select="reverse(for $x in $toDelete return index-of($rend,$x))"/>
        
        
        <!-- then start with kicking out or not -->
        <xsl:choose>
            <!-- if there is nothing to kick out, return rend list -->
            <xsl:when test="empty($kickIndex)">
                <xsl:attribute name="rend">
                    <xsl:value-of select="string-join($rend,';')"/>
                </xsl:attribute>
            </xsl:when>
            <!-- else: build a new list without the unwanted items and then return the new list -->
            <xsl:otherwise>
                <!-- First, tokenize the kickList, but in reversed order to start at the end -->
                <!-- <xsl:variable name="kickList" select="reverse(tokenize($kickIndex,';'))"/>-->
                <!-- Then create a new list with the kickList -->
                <xsl:variable name="cleanedRend" select="for $x in 1 to count($rend) return (if (empty(index-of($kickIndex,$x))) then $rend[$x] else  ' ')"/>
                 
                 
                <!-- throw unnecessary empty elements away and format a proper string -->
                <xsl:variable name="cleanRendString" select="replace(string-join($cleanedRend,';'),' ;','')"/>
                <!-- let's see, if there are still rends left -->
                <xsl:choose>
                    <xsl:when test="$cleanRendString=' ' or $cleanRendString=''">
                        <!-- do nothing -->
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="rend">
                            <xsl:value-of select="$cleanRendString"/>
                        </xsl:attribute>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <!-- Clean empty rend elements (without text and children) -->
    <xsl:template match="tei:hi[not(text()) and not(child::*)]"/>
    
    <!-- wenn Kinder von body nicht p sind, dann soll ein p um die benachbarten Kinder gesetzt werden -->
    <xsl:template match="tei:body">
        <xsl:copy>
            <xsl:for-each-group select="*" group-adjacent="not(name() ='p')">
                <xsl:choose>
                    <xsl:when test="current-group()/name()='p'">
                        <xsl:apply-templates select="current-group()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:apply-templates select="current-group()"/>
                        </xsl:element>
                    </xsl:otherwise>  
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>
    
   </xsl:stylesheet>