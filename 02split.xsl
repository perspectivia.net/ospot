<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0">

    <xsl:output indent="yes" method="xml" encoding="UTF-16"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>

    <xsl:variable name="editor1" select="'Dr. Neslihan Demirkol'"/>
    <xsl:variable name="editor2" select="'Dr. Malek Sharif'"/>
    <xsl:variable name="director" select="'Prof. Dr. Ralf Martin Jäger'"/>
    <xsl:variable name="edition" select="'First Edition'"/>
    <xsl:variable name="respStatement" select="'Editors'"/>
    <xsl:variable name="nameEditionStmt1" select="$editor1"/>
    <xsl:variable name="nameEditionStmt2" select="$editor2"/>
    <xsl:variable name="versions" select="'1'"/>
    <xsl:variable name="identifier" select="'splitted'"/>
    <xsl:variable name="versionLabelEN"
        select="'Splitted document created by the OsPoT transformation routine from CTE to TEI.'"/>
    <xsl:variable name="versionLabelDE"
        select="'Gesplittetes Dokument, das von der OsPoT-Transformationsroutine von CTE nach TEI erstellt wurde.'"/>
    <xsl:variable name="versionLabelTR"
        select="'CTE´den TEI´ye dönüştürme rutini OsPoT tarafından oluşturulan bölünmüş belge.'"/>
    
    <xsl:template match="tei:body">
        <!-- get a variable with all the p that are children of body -->
        <xsl:variable name="pieces">
            <!-- group them according to "piece no" in descendant hi elements -->
            <xsl:for-each-group select="*"
                group-starting-with="tei:p[descendant::tei:hi[contains(., 'piece no')]]">
                <!-- store piece no in a variable -->
                <xsl:variable name="pieceNo"
                    select="replace(.//tei:hi[contains(., 'piece no.')], '\W', '')"/>
                <!-- put an @n attribute with the piece no to each p in the current group -->
                <xsl:for-each select="current-group()">
                    <xsl:copy>
                        <xsl:attribute name="n" select="$pieceNo"/>
                        <xsl:apply-templates select="@* | node()"/>
                    </xsl:copy>
                </xsl:for-each>
            </xsl:for-each-group>
        </xsl:variable>

        <!-- Pieces are grouped here according to the existence of @n at a p within body.
            If a p contains a @n, this is the start of a new piece.
        -->
        <xsl:for-each-group select="$pieces/tei:p" group-adjacent="@n">
            <!-- split the pieces according to the added @n attribute into separate files, using current-grouping-key() as file name -->
            <xsl:result-document href="{concat(current-grouping-key(), '.xml')}" method="xml"
                encoding="UTF-16">

                <!-- write TEI basic structure as hard code and put pieces in body -->
                <xsl:element name="TEI" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:element name="teiHeader" namespace="http://www.tei-c.org/ns/1.0">
                        <xsl:element name="fileDesc" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:element name="titleStmt" namespace="http://www.tei-c.org/ns/1.0">

                                <xsl:element name="title" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="type">
                                        <xsl:value-of select="'desc'"/>
                                    </xsl:attribute>
                                    <!--
                                    <xsl:element name="title" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'original'"/>
                                        </xsl:attribute>
                                    </xsl:element>
                                    -->
                                    <!--
                                    <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'makamOriginal'"/>
                                        </xsl:attribute>
                                    </xsl:element>
                                    -->
                                    <xsl:element name="title" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'titleTranscription'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="xml:lang">
                                            <xsl:value-of select="'ota'"/>
                                        </xsl:attribute>
                                    </xsl:element>
                                    <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'makamTranscription'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="xml:lang">
                                            <xsl:value-of select="'ota'"/>
                                        </xsl:attribute>
                                    </xsl:element>
                                    <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'genreTranscription'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="xml:lang">
                                            <xsl:value-of select="'ota'"/>
                                        </xsl:attribute>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of select="'genreGlossary.xml##[xml:id]'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'[Enter type of genre for this piece or delete elements.]'"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'usulTranscription'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="xml:lang">
                                            <xsl:value-of select="'ota'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'[Enter type of usul for this piece or delete elements.]'"/>
                                    </xsl:element>
                                    <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'poeticForm'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="xml:lang">
                                            <xsl:value-of select="'ota'"/>
                                        </xsl:attribute>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of select="'poeticGlossary.xml##[xml:id]'"/>
                                            </xsl:attribute>
                                            <xsl:element name="supplied" namespace="http://www.tei-c.org/ns/1.0">
                                                <xsl:attribute name="reason">
                                                    <xsl:value-of select="'provided-by-editor'"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="cert">
                                                    <xsl:value-of select="'1'"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="resp">
                                                    <xsl:value-of select="'#ND'"/>
                                                </xsl:attribute>
                                                <xsl:value-of select="'[Enter type of poetic form for this piece or delete elements.]'"/>
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'poeticGenre'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="xml:lang">
                                            <xsl:value-of select="'ota'"/>
                                        </xsl:attribute>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of select="'poeticGlossary.xml##[xml:id]'"/>
                                            </xsl:attribute>
                                            <xsl:element name="supplied" namespace="http://www.tei-c.org/ns/1.0">
                                                <xsl:attribute name="reason">
                                                    <xsl:value-of select="'provided-by-editor'"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="cert">
                                                    <xsl:value-of select="'1'"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="resp">
                                                    <xsl:value-of select="'#ND'"/>
                                                </xsl:attribute>
                                                <xsl:value-of select="'[Enter type of poetic genre for this piece or delete elements.]'"/>
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'bahir'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="xml:lang">
                                            <xsl:value-of select="'ota'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="corresp">
                                            <xsl:value-of select="'[#vezin@xml:id]'"/>
                                        </xsl:attribute>
                                        <xsl:element name="supplied" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="reason">
                                                <xsl:value-of select="'provided-by-editor'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="cert">
                                                <xsl:value-of select="'1'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="resp">
                                                <xsl:value-of select="'#ND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'[Enter type of bahir for this piece and multiply if necessary or delete elements.]'"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'meter'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="xml:lang">
                                            <xsl:value-of select="'ota'"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="corresp">
                                            <xsl:value-of select="'[#feet@xml:id]'"/>
                                        </xsl:attribute>
                                        <xsl:element name="supplied" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="reason">
                                                <xsl:value-of select="'provided-by-editor'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="cert">
                                                <xsl:value-of select="'1'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="resp">
                                                <xsl:value-of select="'#ND'"/>
                                            </xsl:attribute>
                                                <xsl:value-of select="'[Enter type of meter structure for this piece and multiply if necessary or delete elements.]'"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="'RISM'"/>
                                        </xsl:attribute>
                                    </xsl:element>

                                </xsl:element>
                                <xsl:element name="author" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="role"/>
                                    <xsl:attribute name="cert">
                                        <xsl:value-of select="'[0-1]'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="resp">
                                        <xsl:value-of select="'[#Initials]'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'[Enter name of author here and multiply if necessary or delete elements.]'"/>
                                </xsl:element>
                                <xsl:element name="editor" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="role">
                                        <xsl:value-of select="'author'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="corresp">
                                        <xsl:value-of select="'#ND'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="$editor1"/>
                                </xsl:element>
                                <xsl:element name="editor" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="role">
                                        <xsl:value-of select="'contributor'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="corresp">
                                        <xsl:value-of select="'#MS'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="$editor2"/>
                                </xsl:element>
                                <xsl:element name="editor" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="role"/>
                                    <xsl:value-of select="'[Add additional name and role if necessary with either @corresp or @ref or delete element.]'"/>
                                </xsl:element>
                            </xsl:element>
                            
                            <!-- Edition statement -->
                            
                            <xsl:element name="editionStmt" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:element name="edition" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="$edition"/>
                                </xsl:element>
                                <xsl:element name="funder" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="ref">
                                        <xsl:value-of select="'https://www.dfg.de'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'Deutsche Forschungsgemeinschaft'"/>
                                </xsl:element>
                                <xsl:element name="sponsor" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="ref">
                                        <xsl:value-of select="'https://www.wwu.de'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'Westfälische Wilhelms-Universität Münster'"/>
                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'ged'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'General Editor'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#RJ'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="ref">
                                                <xsl:value-of
                                                  select="'https://explore.gnd.network/gnd/122654099'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="$director"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of
                                                select="'Westfälische Wilhelms-Universität Münster'"
                                            />
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'edi'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="$respStatement"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#ND'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="ref">
                                                <xsl:value-of
                                                  select="'https://orcid.org/0000-0002-8602-1704'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="$editor1"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:element name="persName"
                                            namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:id">
                                                <xsl:value-of select="'MS'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="ref">
                                                <xsl:value-of
                                                  select="'https://explore.gnd.network/gnd/106049969X'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="$editor2"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            
                            <!-- Publication statement -->
                            
                            <xsl:element name="publicationStmt" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:element name="publisher" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'Corpus Musicae Ottomanicae'"/>
                                </xsl:element>
                                <xsl:element name="distributor"
                                    namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'perspectiva.net - Die Publikationsplattform der Max Weber Stiftung'"/>
                                </xsl:element>
                                <xsl:element name="date" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'2021'"/>
                                </xsl:element>
                                <xsl:element name="pubPlace" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'online'"/>
                                </xsl:element>
                                <xsl:element name="availability" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="licence" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Licence: Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)'"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <!--
                            <xsl:element name="notesStmt" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0"/>
                            </xsl:element>
                            -->
                            
                            <!-- Series statement -->
                            
                            <xsl:element name="seriesStmt" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:element name="title" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'Corpus Musicae Ottomanicae'"/>
                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'edc'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Editor of Digital Corpus'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'CMO'"/>
                                        </xsl:attribute>
                                        <xsl:element name="orgName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:lang">
                                                <xsl:value-of select="'en'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Corpus Musicae Ottomanicae, Research Center of the 
                                                German Research Foundation at the University of Münster, Institute of
                                                Musicology.'"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'oth'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Cooperation Partners'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'MWS'"/>
                                        </xsl:attribute>
                                        <xsl:element name="orgName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Max Weber Stiftung'"/>
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'GND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'1028661126'"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'OII'"/>
                                        </xsl:attribute>
                                        <xsl:element name="orgName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Orient-Institut Istanbul'"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'pdr'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Project Director'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'RJ'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Ralf Martin Jäger'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation"
                                            namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'GND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'122654099'"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'oth'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Research Assistant'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'ZH'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Zeynep Helvacı'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'GND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'1080200312'"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'edt'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Editors'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'NA'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Nejla Melike Atalay'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="idno"
                                            namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'GND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'1244416428'"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'ND'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Neslihan Demirkol'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                              <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                              <xsl:value-of select="'https://orcid.org/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                              <xsl:value-of select="'ORCID'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'0000-0002-8602-1704'"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'MD'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Marco Dimitriou'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                              <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'CM'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'C. Ersin Mıhçı'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                              <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'SP'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Semih Pelen'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                              <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'edt'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Student Assistants'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'MG'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Melek Göksu'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'OO'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Orçun Öztürk'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#CMO'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                    </xsl:element>                                </xsl:element>
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'led'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Head of Section perspectivia.net, IT, Library Affairs'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'MK'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Michael Kaiser'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#MWS'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'GND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'139213147'"/>
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://orcid.org/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'ORCID'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'0000-0001-9520-8119'"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>                                
                                <xsl:element name="respStmt" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="resp" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="key">
                                            <xsl:value-of select="'oth'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Research Managers Digital Editions and Data Management'"/>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'SG'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Sven Gronemeyer'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#MWS'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="affiliation" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'La Trobe University, Melbourne'"
                                            />
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'GND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'1155600487'"/>
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://orcid.org/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'ORCID'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'0000-0002-9066-0461'"/>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="name" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="xml:id">
                                            <xsl:value-of select="'JS'"/>
                                        </xsl:attribute>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:value-of select="'Julian Schulz'"/>
                                        </xsl:element>
                                        <xsl:element name="affiliation"
                                            namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#MWS'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="xml:base">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="type">
                                                <xsl:value-of select="'GND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'1161130233'"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="sourceDesc" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:element name="ab" namespace="http://www.tei-c.org/ns/1.0"/>
                            </xsl:element>
                        </xsl:element>
                        
                        <!-- Encoding description -->
                        
                        <xsl:element name="encodingDesc" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:element name="variantEncoding" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="method">double-end-point</xsl:attribute>
                                <xsl:attribute name="location">external</xsl:attribute>
                            </xsl:element>
                            
                            <!-- Application information EN -->
                            
                            <xsl:element name="appInfo" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'en'"/>
                                </xsl:attribute>
                                <xsl:element name="application"
                                    namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="version">
                                        <xsl:value-of select="$versions"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="ident">
                                        <xsl:value-of select="$identifier"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="when">
                                        <xsl:value-of select="current-dateTime()"/>
                                    </xsl:attribute>
                                    <xsl:element name="label"
                                        namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of select="$versionLabelEN"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'This file is the result of a TEI transformation from XML output of the '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://cte.oeaw.ac.at/'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Classical Text Editor'"/>
                                        </xsl:element>
                                        <xsl:value-of
                                            select="' (CTE) created by the '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://gitlab.gwdg.de/perspectivia.net/ospot'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'OsPoT Framework'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'. The modelling is based on the '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://www.tei-c.org/release/doc/tei-p5-doc/en/html/VE.html'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'verse section of the TEI Guidelines'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'.'"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of select="'The transformation was created by '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="ref">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/1229022201'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Anna Plaksin'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' and '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="ref">
                                                <xsl:value-of select="'https://orcid.org/0000-0001-8640-0213'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Frauke Pirk'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' and was enhanced by '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#SG'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Sven Gronemeyer'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'. It was developed in close consultation with '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#ND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Neslihan Demirkol'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' to model the needs for the digital scholarly edition.'"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'Further information on the transformation is available in the '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of select="'https://www.uni-muenster.de/CMO-Edition/en/publikationen/publikationen.html'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'editorial commentary'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'.'"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'The metadata was retrieved from the project´s own '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://corpus-musicae-ottomanicae.de/content/index.xml'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'source catalogue and publication platform'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'.'"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <!-- Application information DE -->
                            
                            <xsl:element name="appInfo" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'de'"/>
                                </xsl:attribute>
                                <xsl:element name="application"
                                    namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="version">
                                        <xsl:value-of select="$versions"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="ident">
                                        <xsl:value-of select="$identifier"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="when">
                                        <xsl:value-of select="current-dateTime()"/>
                                    </xsl:attribute>
                                    <xsl:element name="label"
                                        namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of select="$versionLabelDE"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'Diese Datei ist das Ergebnis einer TEI-Transformation aus der XML-Ausgabe des '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://cte.oeaw.ac.at/'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Classical Text Editor'"/>
                                        </xsl:element>
                                        <xsl:value-of
                                            select="' (CTE), erstellt vom '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://gitlab.gwdg.de/perspectivia.net/ospot'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'OsPoT Framework'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'. Die Modellierung basiert auf dem '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://www.tei-c.org/release/doc/tei-p5-doc/en/html/VE.html'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Abschnitt über Verse in den TEI-Richtlinien'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'.'"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of select="'Die Transformation wurde von '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="ref">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/1229022201'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Anna Plaksin'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' und '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="ref">
                                                <xsl:value-of select="'https://orcid.org/0000-0001-8640-0213'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Frauke Pirk'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' erstellt und erweitert durch '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#SG'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Sven Gronemeyer'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'. Sie wurde in enger Konsultation mit '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#ND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Neslihan Demirkol'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' entwickelt, um die Bedarfe für eine digitale kritische Edition zu modellieren.'"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'Weitere Informationen zur Transformation finden Sie im '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of select="'https://www.uni-muenster.de/CMO-Edition/en/publikationen/publikationen.html'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'editorischen Kommentar'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'.'"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'Die Metadaten wurden abgerufen aus dem projekteigenen '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://corpus-musicae-ottomanicae.de/content/index.xml'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Quellenkatalog und der Publikationsplattform'"/>
                                        </xsl:element>
                                        <xsl:value-of select="'.'"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <!-- Application information TR -->

                            <xsl:variable name="quotationMark">'</xsl:variable>
                            <xsl:element name="appInfo" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'tr'"/>
                                </xsl:attribute>
                                <xsl:element name="application"
                                    namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="version">
                                        <xsl:value-of select="$versions"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="ident">
                                        <xsl:value-of select="$identifier"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="when">
                                        <xsl:value-of select="current-dateTime()"/>
                                    </xsl:attribute>
                                    <xsl:element name="label"
                                        namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of select="$versionLabelTR"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'Bu dosya, '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://cte.oeaw.ac.at/'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Classical Text Editor'"/>
                                        </xsl:element>
                                        <xsl:value-of
                                            select="' (CTE) programından alınan XML çıktısının '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://gitlab.gwdg.de/perspectivia.net/ospot'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'OsPoT Framework'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' tarafından uygulanan TEI dönüştürme rutini sonucunda oluşturulmuştur. Modelleme, '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://www.tei-c.org/release/doc/tei-p5-doc/en/html/VE.html'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'TEI Kılavuzunun nazım bölümü'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' ne göre biçimlendirilmiştir.'"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of select="'TEI dönüştürme rutini, '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="ref">
                                                <xsl:value-of select="'https://explore.gnd.network/gnd/1229022201'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Anna Plaksin'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' ve '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="ref">
                                                <xsl:value-of select="'https://orcid.org/0000-0001-8640-0213'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Frauke Pirk'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' tarafından hazırlanmış, '"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#SG'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Sven Gronemeyer'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' tarafından geliştirilmiştir. Dijital akademik edisyon için gerekli olarak özelliklere göre modellenebilmesi için'"/>
                                        <xsl:element name="persName" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="corresp">
                                                <xsl:value-of select="'#ND'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'Dr. Neslihan Demirkol'"/>
                                        </xsl:element>
                                        <xsl:value-of select="$quotationMark"/>
                                        <xsl:text>la yakın işbirliği içinde geliştirilmiştir.</xsl:text>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'TEI dönüştürme rutini ile ilgili ayrıntılı bilgiyi '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of select="'https://www.uni-muenster.de/CMO-Edition/en/publikationen/publikationen.html'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'editoryal yorumlar'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' bölümünde bulabilirsiniz.'"/>
                                    </xsl:element>
                                    <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="'Meta veriler projenin kendi '"/>
                                        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="target">
                                                <xsl:value-of
                                                    select="'https://corpus-musicae-ottomanicae.de/content/index.xml'"/>
                                            </xsl:attribute>
                                            <xsl:value-of select="'kaynak kataloğu ve yayın platformundan'"/>
                                        </xsl:element>
                                        <xsl:value-of select="' alınmıştır.'"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            
                            <!-- Project description EN -->
                            
                            <xsl:element name="projectDesc" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'en'"/>
                                </xsl:attribute>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'Corpus Musicae Ottomanicae (CMO) is a project funded by the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.dfg.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Deutsche Forschungsgemeinschaft'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' (DFG,
                                            German Research Foundation). The CMO project is carried out collaboratively
                                            across three locations: Münster and Bonn (both in Germany) and Istanbul (Turkey).
                                            Each center deals with a specific aspect of the project. The central research
                                            task – the transcription and critical editing of nineteenth-century sources of
                                            Ottoman music – is carried out at the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/Musikwissenschaft/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Institute for Musicology'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' at the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.wwu.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Westfälische Wilhelms-Universität'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' in Münster. Critical editions of the accompanying 
                                        texts of vocal pieces are prepared in collaboration with '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/ArabistikIslam/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Institute of Arabic and Islamic Studies'"/>
                                    </xsl:element>
                                    <xsl:value-of select="', also at the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.wwu.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Westfälische Wilhelms-Universität'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' in Münster. The project support and digital publication of
                                        the edition is undertaken jointly by associates based at the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.maxweberstiftung.de/en/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Max Weber Foundation'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' in Bonn and the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.oiist.org/en/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Orient-Institut Istanbul'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'For further information on the project, please visit the following page: '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of
                                                select="'https://www.uni-muenster.de/CMO-Edition/en/cmo/beschreibung.html'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Project Description'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'For an overview of the staff involved, please visit the following page: '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of
                                                select="'https://corpus-musicae-ottomanicae.de/content/below/contributors.xml'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Contributors'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                            </xsl:element>
                            
                            <!-- Project description DE -->
                            
                            <xsl:element name="projectDesc" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'de'"/>
                                </xsl:attribute>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'Corpus Musicae Ottomanicae (CMO) ist ein von der '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.dfg.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Deutschen Forschungsgemeinschaft'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' finanziertes
                                        Projekt. Das CMO-Projekt wird in Zusammenarbeit an drei Standorten durchgeführt:
                                        Münster und Bonn (beide in Deutschland) und Istanbul (Türkei). Jedes Zentrum
                                        beschäftigt sich mit einem spezifischen Aspekt des Projekts. Die zentrale
                                        Forschungsaufgabe - die Transkription und kritische Edition von Quellen
                                        osmanischer Musik aus dem 19. Jahrhundert - wird am '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/Musikwissenschaft/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Institut für Musikwissenschaft'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' der '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.wwu.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Westfälischen Wilhelms-Universität'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' in Münster durchgeführt. Kritische Editionen
                                        der Begleittexte von Vokalstücken werden in Zusammenarbeit mit dem '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/ArabistikIslam/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Institut für Arabistik und Islamwissenschaft'"/>
                                    </xsl:element>
                                    <xsl:value-of select="', ebenfalls an der '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.wwu.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Westfälischen Wilhelms-Universität'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' in Münster, erstellt. Die Projektbegleitung und 
                                        digitale Publikation der Ausgabe erfolgt gemeinsam mit Mitarbeitenden der '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.maxweberstiftung.de/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Max Weber Stiftung'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' in Bonn und des '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.oiist.org/en/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Orient-Instituts Istanbul'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'Weitere Informationen zum Projekt finden Sie auf der folgenden Seite: '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of
                                                select="'https://www.uni-muenster.de/CMO-Edition/cmo/beschreibung.html'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Projektbeschreibung'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'Einen Überblick über die beteiligten Mitarbeitenden finden Sie auf der folgenden Seite: '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of
                                                select="'https://corpus-musicae-ottomanicae.de/content/below/contributors.xml'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Mitarbeitende'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                            </xsl:element>

                            <!-- Project description TR -->
                            
                            <xsl:element name="projectDesc" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'tr'"/>
                                </xsl:attribute>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'Corpus Musicae Ottomanicae (CMO), '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.dfg.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Deutsche Forschungsgemeinschaft'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' (DFG, Alman Araştırma Cemiyeti) tarafından
                                        finanse edilen bir projedir. CMO projesi, üç farklı merkezde bulunan kurum ve
                                        kuruluşlar tarafından ortaklaşa yürütülmektedir: Münster (Almanya), Bonn (Almanya)
                                        ve İstanbul (Türkiye). Her merkez projenin belirli bir yönüyle ilgi araştırmayı
                                        yürütmektedir. Projenin ana araştırma hedefi olan on dokuzuncu yüzyıl Osmanlı
                                        müziği kaynaklarının transkripsiyonu ve eleştirel edisyonu, Münster´deki '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.wwu.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Westfälische Wilhelms-Universität'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' bünyesindeki '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/Musikwissenschaft/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Müzikoloji Enstitüsü'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' nde yürütülmektedir. Sözlü eserlere eşlik eden metinlerin eleştirel
                                        edisyonları, yine Münster´deki '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.wwu.de'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Westfälische Wilhelms-Universität'"/>
                                    </xsl:element>
                                    <xsl:value-of select="', bünyesindeki '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/ArabistikIslam/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Arap ve İslam Araştırmaları Enstitüsü'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' ile işbirliği içinde hazırlanmaktadır. Bonn´da bulunan '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.maxweberstiftung.de/en/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Max Weber Vakfı'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' ve İstanbul´da bulunan '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.oiist.org/en/'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Orient-Institute Istanbul'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'projeye destek sağlama ve dijital yayın görevlerini ortaklaşa üstlenmektedir.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'Projeyle ilgili ayrıntılı bilgiyi '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of
                                                select="'https://www.uni-muenster.de/CMO-Edition/en/cmo/beschreibung.html'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Proje Açıklaması'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' sayfasında bulabilirsiniz.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="'Projede görev alan personele dair ayrıntılı bilgiyi '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of
                                                select="'https://corpus-musicae-ottomanicae.de/content/below/contributors.xml'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Katkıda Bulunanlar'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' sayfasında bulabilirsiniz.'"/>
                                </xsl:element>
                            </xsl:element>

                            <!-- Editorial declaration EN -->
                            
                            <xsl:element name="editorialDecl" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'en'"/>
                                </xsl:attribute>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'An explanation of the editorial process can be found in the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://corpus-musicae-ottomanicae.de/content/help/user_guide.xml'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'editorial guidelines'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' and the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/CMO-Edition/en/publikationen/publikationen.html'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'commentary'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'An explanation of the transcription process of Ottoman
                                        lyrics from Arabic letters into Latin characters can be found in the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/imperia/md/content/cmo-edition/publikationen/guidelines_for_transcription_ver002.pdf'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'transcription guidelines'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'A summary of the standardized terms of Ottoman music can be found in the '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/imperia/md/content/cmo-edition/publikationen/standard_list_of_musical_terms_revised_edition.pdf'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'list of musical terms'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                            </xsl:element>
                            
                            <!-- Editorial declaration DE -->
                            
                            <xsl:element name="editorialDecl" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'de'"/>
                                </xsl:attribute>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'Eine Erläuterung des Editionsprozesses finden Sie in den '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://corpus-musicae-ottomanicae.de/content/help/user_guide.xml'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'editorischen Leitlinien'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' und dem '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/CMO-Edition/en/publikationen/publikationen.html'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Kommentar'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'Eine Erläuterung der Transkription osmanischer Texte aus
                                        der arabischen Schrift in lateinische Buchstaben findet sich in den '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/imperia/md/content/cmo-edition/publikationen/guidelines_for_transcription_ver002.pdf'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Transkriptionsrichtlinien'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'Eine Übersicht über die standardisierten Begriffe der osmanischen Musik findet sich in der '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/imperia/md/content/cmo-edition/publikationen/standard_list_of_musical_terms_revised_edition.pdf'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'Liste der musikalischen Begriffe'"/>
                                    </xsl:element>
                                    <xsl:value-of select="'.'"/>
                                </xsl:element>
                            </xsl:element>

                            <!-- Editorial declaration TR -->
                            
                            <xsl:element name="editorialDecl" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:lang">
                                    <xsl:value-of select="'tr'"/>
                                </xsl:attribute>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'Editoryal sürecin ayrıntılarına dair bilgileri '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://corpus-musicae-ottomanicae.de/content/help/user_guide.xml'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'editoryal yönergeler'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' ve '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/CMO-Edition/en/publikationen/publikationen.html'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'yorum'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' sayfalarında bulabilirsiniz.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'Osmanlı Türkçesindeki şarkı sözlerinin Osmanlı alfabesinden modern
                                        Türkçe alfabesine transkripsiyon sürecine ilişkin açıklamaları '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/imperia/md/content/cmo-edition/publikationen/guidelines_for_transcription_ver002.pdf'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'transkripsiyon kılavuzu'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' nda bulabilirsiniz.'"/>
                                </xsl:element>
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="'Osmanlı müziğinin standartlaştırılmış terimlerinin bir özetini '"/>
                                    <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="'https://www.uni-muenster.de/imperia/md/content/cmo-edition/publikationen/standard_list_of_musical_terms_revised_edition.pdf'"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="'müzik terimleri listesi'"/>
                                    </xsl:element>
                                    <xsl:value-of select="' nde bulabilirsiniz.'"/>
                                </xsl:element>
                            </xsl:element>

                            <!-- Taagging declaration -->
                            
                            <xsl:element name="tagsDecl" namespace="http://www.tei-c.org/ns/1.0"/>
                            
                            <!-- Metrical notation declaration -->
                            
                            <xsl:element name="metDecl" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:id">
                                    <xsl:value-of select="'symbols'"/>
                                </xsl:attribute>
                                <xsl:attribute name="pattern">
                                    <xsl:value-of select="'((u|-|x|U|Z|I|M){1,5}[|]){1,4}'"/>
                                </xsl:attribute>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'u'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'u'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'short syllable which can be in the form of cv or v'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'-'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'-'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'long syllable which can be in the form of cV, cVc, cvc, cVcc, cvcc, vc, or V'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'x'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'x'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'anceps, the position in the metrical pattern can either be short or long'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'U'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'U'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'merge (ulama/vasl)'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'Z'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'Z'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'shortening (zihâf/kısaltma)'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'I'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'I'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'lengthening (imâle/uzatma)'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'M'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'M'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'insertion (med)'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'|'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'|'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'foot division'"/>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="metDecl" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:id">
                                    <xsl:value-of select="'feet'"/>
                                </xsl:attribute>
                                <xsl:attribute name="corresp">
                                    <xsl:value-of select="'#symbols'"/>
                                </xsl:attribute>
                                <xsl:attribute name="type">
                                    <xsl:value-of select="'met'"/>
                                </xsl:attribute>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'fa'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fâ’'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'-|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'failatu'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fâ’ilâtü'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'-u-u|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'failatun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fâ’ilâtün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'-u--|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'failun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fâ’ilün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'-u-|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'falun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fâ’lün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'--|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'feilatun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fe’ilâtün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'uu--|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'feilun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fe’ilün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'uu-|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'feul'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fe’ûl'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'u-|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'feulun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'fe’ûlün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'u--|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mefulu'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'mef’ûlü'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'--u|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mefulun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'mef’ûlün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'---|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mefailu'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'mefâ’îlü'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'u--u|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mefailun1'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'mefâ’ilün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'u-u-|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mefailun2'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'mefâ’îlün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'u---|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mufteilatun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'müfte’ilâtün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'-uu--|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mufteilun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'müfte’ilün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'-uu-|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mustefilun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'müstef’ilün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'--u-|'"/>
                                </xsl:element>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'mutefailun'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="'mütefâ’ilün'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'uu-u-|'"/>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="metDecl" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="xml:id">
                                    <xsl:value-of select="'vezin'"/>
                                </xsl:attribute>
                                <xsl:attribute name="corresp">
                                    <xsl:value-of select="'#symbols'"/>
                                </xsl:attribute>
                                <xsl:attribute name="type">
                                    <xsl:value-of select="'met'"/>
                                </xsl:attribute>
                                <xsl:element name="metSym" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="xml:id">
                                        <xsl:value-of select="'[#vezin@xml:id]'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value"/>
                                    <xsl:attribute name="terminal">
                                        <xsl:value-of select="'false'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'[Enter standard syllable sequence here and multiply element if necessary or delete element.]'"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                    
                    <!-- Text, body and apparatus -->
                    
                    <xsl:element name="text" namespace="http://www.tei-c.org/ns/1.0">
                        <xsl:element name="body" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:copy-of select="current-group()"/>
                        </xsl:element>
                        <xsl:element name="back" namespace="http://www.tei-c.org/ns/1.0"/>
                    </xsl:element>
                </xsl:element>
            </xsl:result-document>
        </xsl:for-each-group>
    </xsl:template>


</xsl:stylesheet>
