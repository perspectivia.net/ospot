<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:mei="http://www.music-encoding.org/ns/mei"
    version="3.0">

    <xsl:output indent="yes" method="xml" encoding="UTF-16"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:strip-space elements="*"/>


    <xsl:variable name="witnesses" select="//tei:note[@type = 'a2']//tei:abbr[@n]"/>
    <xsl:variable name="baseUri" select="'https://corpus-musicae-ottomanicae.de/receive/'"/>

    <xsl:template match="tei:sourceDesc//tei:ab">
        <xsl:element name="listWit" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:element name="witness" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="xml:id">
                    <xsl:call-template name="getIDfromCatalogue">
                        <xsl:with-param name="sigla" select="'NE204'"/>
                        <xsl:with-param name="prelimID" select="'bllaaaaa'"/>
                    </xsl:call-template>
                </xsl:attribute>
                <xsl:call-template name="buildSourceDesc">
                    <xsl:with-param name="sigla" select="'NE204'"/>
                </xsl:call-template>
            </xsl:element>
            <xsl:for-each select="$witnesses">
                <xsl:element name="witness" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="xml:id">
                        <xsl:call-template name="getIDfromCatalogue">
                            <xsl:with-param name="sigla" select="."/>
                            <xsl:with-param name="prelimID" select="@n"/>
                        </xsl:call-template>
                    </xsl:attribute>
                    <xsl:call-template name="buildSourceDesc">
                        <xsl:with-param name="sigla" select="."/>
                    </xsl:call-template>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template name="getIDfromCatalogue">
        <xsl:param name="sigla"/>
        <xsl:param name="prelimID"/>
        <xsl:variable name="queryUriStart"
            select="'https://corpus-musicae-ottomanicae.de/servlets/solr/select?core=main&amp;q=identifier.type.CMO%3A'"/>
        <xsl:variable name="queryUriEnd" select="'&amp;wt=xml&amp;XSL.Style=xml'"/>

        <!-- query ID by sigla from Source Catalogue -->
        <xsl:variable name="id"
            select="document(concat($queryUriStart, $sigla, $queryUriEnd))//result/doc/str[@name = 'id']/text()"/>
        <!-- add ID from catalogue if possible, otherwise stick with preliminary ID -->
        <xsl:choose>
            <xsl:when test="$id != ''">
                <xsl:value-of select="$id"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$prelimID"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="buildSourceDesc">
        <xsl:param name="sigla"/>

        <!--xsl:variable name="cmoClasses" select="'https://corpus-musicae-ottomanicae.de/api/v1/classifications/'"/>-->
        <xsl:variable name="id">
            <xsl:call-template name="getIDfromCatalogue">
                <xsl:with-param name="sigla" select="$sigla"/>
                <xsl:with-param name="prelimID" select="''"/>
            </xsl:call-template>
        </xsl:variable>

        <!-- give Sigla -->
        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="type">
                <xsl:value-of select="'CMO'"/>
            </xsl:attribute>
            <xsl:value-of select="$sigla"/>
        </xsl:element>
        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="type">
                <xsl:value-of select="'RISM'"/>
            </xsl:attribute>
            <!--<xsl:value-of select="$sigRISM"/>-->
        </xsl:element>
        <xsl:if test="$id != ''">
            <!-- build sourceDesc -->
            <!-- get data from source catalogue -->
            <xsl:variable name="catEntry" select="document(concat($baseUri, $id, '?XSL.Style=mei'))"/>

            <xsl:element name="biblFull" namespace="http://www.tei-c.org/ns/1.0">
                <!-- fileDesc contains titleStmt, publicationStmt, seriesStmt, notesStmt and sourceDesc -->
                <xsl:element name="fileDesc" namespace="http://www.tei-c.org/ns/1.0">
                    <!-- titlestmt -->
                    <xsl:element name="titleStmt" namespace="http://www.tei-c.org/ns/1.0">
                        <!-- if data has mei:titleStmt, get data, otherwise insert an empty title -->
                        <!-- make sure, that everything inside componentGrp is not queried -->
                        <xsl:choose>
                            <xsl:when
                                test="$catEntry//mei:titleStmt[not(ancestor::mei:componentList)]">
                                <xsl:for-each
                                    select="$catEntry//mei:titleStmt[not(ancestor::mei:componentList)]/element()">
                                    <xsl:call-template name="copyMeiElement">
                                        <xsl:with-param name="element" select="."/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:element name="title" namespace="http://www.tei-c.org/ns/1.0"/>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:element name="author" namespace="http://www.tei-c.org/ns/1.0"/>
                    </xsl:element>
                    <!-- publicationStmt -->
                    <xsl:element name="publicationStmt" namespace="http://www.tei-c.org/ns/1.0">
                        <xsl:choose>
                            <xsl:when test="$catEntry//mei:pubStmt">
                                <xsl:if test="not(tei:publisher or tei:ab)">
                                    <xsl:element name="publisher"
                                        namespace="http://www.tei-c.org/ns/1.0"/>
                                </xsl:if>
                                <xsl:for-each select="$catEntry//mei:pubStmt/element()">
                                    <xsl:call-template name="copyMeiElement">
                                        <xsl:with-param name="element" select="."/>
                                    </xsl:call-template>
                                </xsl:for-each>
                            </xsl:when>
                            <!-- In case of unpublished manuscript, put ab into -->
                            <xsl:otherwise>
                                <xsl:element name="ab" namespace="http://www.tei-c.org/ns/1.0"/>
                            </xsl:otherwise>
                        </xsl:choose>

                    </xsl:element>
                    <!-- seriesStmt is quasi identical -->
                    <xsl:if test="$catEntry//mei:seriesStmt">
                        <xsl:call-template name="copyMeiElement">
                            <xsl:with-param name="element" select="$catEntry//mei:seriesStmt"/>
                        </xsl:call-template>
                    </xsl:if>
                    <!-- notesStmt: may be available -->
                    <xsl:if test="$catEntry//mei:notesStmt">
                        <xsl:element name="notesStmt" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:for-each select="$catEntry//mei:notesStmt/element()">
                                <xsl:call-template name="copyMeiElement">
                                    <xsl:with-param name="element" select="."/>
                                </xsl:call-template>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:if>
                    <!-- sourceDesc contains reference to CMO Source Catalogue -->
                    <xsl:element name="sourceDesc" namespace="http://www.tei-c.org/ns/1.0">
                        <xsl:element name="bibl" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:value-of select="'Source description originates from CMO Source Catalogue: '"/> 
                            <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="type" select="'uri'"/>
                                <xsl:value-of select="concat($baseUri, $id)"/>
                            </xsl:element>
                            <xsl:value-of select="'.'"/>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>

                <!-- profileDesc contains classifications and language info -->
                <xsl:element name="profileDesc" namespace="http://www.tei-c.org/ns/1.0">
                    <!-- add creation date here, if available -->
                    <!-- because of the old event structure of <history> it is not possible to mix them, even if it
                        would be preferable to have the creation date within msDesc -->
                    <xsl:if test="$catEntry//mei:creation">
                        <xsl:element name="creation" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:call-template name="copyMeiElement">
                                <xsl:with-param name="element"
                                    select="$catEntry//mei:creation/mei:date"/>
                            </xsl:call-template>
                        </xsl:element>
                    </xsl:if>

                    <!-- classifications: -->
                    <xsl:element name="textClass" namespace="http://www.tei-c.org/ns/1.0">
                        <xsl:element name="keywords" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:for-each select="$catEntry//mei:classification/mei:termList">
                                <!-- get the classification from termList -->
                                <xsl:variable name="classLink" select="./@class"/>
                                <xsl:variable name="classFile"
                                    select="document(replace(./@class, 'https://www.', 'https://'))"/>

                                <!-- Go over each term - there can be more than just one!!! -->
                                <xsl:for-each select="./mei:term">
                                    <!-- get the term ID -->
                                    <xsl:variable name="termID" select="./text()"/>
                                    <!-- build TEI element -->
                                    <xsl:element name="term" namespace="http://www.tei-c.org/ns/1.0">
                                        <!-- provide link to classification entry -->
                                        <xsl:attribute name="target">
                                            <xsl:value-of select="concat($classLink, '#', $termID)"
                                            />
                                        </xsl:attribute>
                                        <!-- provide type of classification -->
                                        <xsl:attribute name="type">
                                            <xsl:value-of
                                                select="substring-after($classLink, 'classifications/')"
                                            />
                                        </xsl:attribute>
                                        <!-- add english term as element value -->
                                        <xsl:value-of
                                            select="$classFile//category[@ID = $termID]/label[@xml:lang = 'en']/@text"
                                        />
                                    </xsl:element>
                                </xsl:for-each>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:element>

                    <!-- not all sources have language definitions -->
                    <xsl:if test="$catEntry//mei:langUsage">
                        <xsl:element name="langUsage" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:for-each select="$catEntry//mei:langUsage/mei:language">
                                <!-- I don't know why, but selecting the class category directly via ./@xml:id doesn't work... then let's have a variable -->
                                <xsl:variable name="langTag" select="./@xml:id"/>
                                <xsl:element name="language" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="ident">
                                        <xsl:value-of select="$langTag"/>
                                    </xsl:attribute>
                                    <xsl:variable name="langClass"
                                        select="document(concat('https://corpus-musicae-ottomanicae.de/api/v1/classifications/', ./@auth))"/>

                                    <xsl:value-of
                                        select="$langClass//category[@ID = $langTag]/label[@xml:lang = 'en']/@text"
                                    />
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:if>
                </xsl:element>
            </xsl:element>

            <!-- start with manuscript description -->
            <xsl:if
                test="$catEntry//mei:physLoc or $catEntry//mei:contents or $catEntry//mei:physDesc or $catEntry//mei:handList or $catEntry//mei:history">
                <xsl:element name="msDesc" namespace="http://www.tei-c.org/ns/1.0">
                    <!-- msIdentifier contains info about location -->
                    <xsl:choose>
                        <xsl:when test="$catEntry//mei:physLoc">
                            <xsl:element name="msIdentifier" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:element name="country" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="$catEntry//mei:repository//mei:geogName[@type = 'country']"
                                    />
                                </xsl:element>
                                <xsl:element name="settlement"
                                    namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="$catEntry//mei:repository//mei:geogName[@type = 'city']"
                                    />
                                </xsl:element>
                                <xsl:element name="repository"
                                    namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="$catEntry//mei:repository/mei:corpName[@type = 'library']"
                                    />
                                </xsl:element>
                                <xsl:if
                                    test="$catEntry//mei:repository/mei:identifier[@type = 'collection']">
                                    <xsl:element name="collection"
                                        namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:value-of
                                            select="$catEntry//mei:repository/mei:identifier[@type = 'collection']"
                                        />
                                    </xsl:element>
                                </xsl:if>
                                <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of
                                        select="$catEntry//mei:repository/mei:identifier[@type = 'shelfmark']"
                                    />
                                </xsl:element>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="msIdentifier" namespace="http://www.tei-c.org/ns/1.0"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                    <!-- Get the content summary -->
                    <xsl:if test="$catEntry//mei:contents">
                        <xsl:element name="msContents" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:element name="summary" namespace="http://www.tei-c.org/ns/1.0">
                                <!--<xsl:value-of select="$catEntry//mei:contents/mei:p"/>-->
                                <xsl:value-of select="translate($catEntry//mei:contents/mei:p, '&#xd;', '')"/>
                            </xsl:element>
                        </xsl:element>
                    </xsl:if>
                    <!-- physDesc -->
                    <xsl:if test="$catEntry//mei:physDesc">
                        <xsl:element name="physDesc" namespace="http://www.tei-c.org/ns/1.0">
                            <!-- get dimensions, extent, info about materials and condition -->
                            <xsl:if
                                test="$catEntry//mei:dimensions or $catEntry//mei:extent or $catEntry//mei:physMedium or $catEntry//mei:condition">
                                <xsl:element name="objectDesc"
                                    namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:element name="supportDesc"
                                        namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:if test="$catEntry//mei:physMedium">
                                            <xsl:element name="support"
                                                namespace="http://www.tei-c.org/ns/1.0">
                                                <xsl:for-each select="$catEntry//mei:physMedium">
                                                  <xsl:element name="material"
                                                  namespace="http://www.tei-c.org/ns/1.0">
                                                  <xsl:value-of select="concat(./@label, ': ', .)"/>
                                                  </xsl:element>
                                                </xsl:for-each>
                                            </xsl:element>
                                        </xsl:if>
                                        <!-- end materials -->
                                        <xsl:if test="$catEntry//mei:extent">
                                            <xsl:element name="extent"
                                                namespace="http://www.tei-c.org/ns/1.0">
                                                <xsl:value-of
                                                  select="concat('Extent: ', $catEntry//mei:extent)"/>
                                                <!-- dimensions is within extent in TEI -->
                                                <xsl:if test="$catEntry//mei:dimensions">
                                                  <!-- dimensions are as H x W in the source catalogue -->
                                                  <xsl:element name="dimensions"
                                                  namespace="http://www.tei-c.org/ns/1.0">
                                                  <xsl:element name="height"
                                                  namespace="http://www.tei-c.org/ns/1.0">
                                                  <xsl:value-of
                                                  select="substring-before($catEntry//mei:dimensions, ' x ')"
                                                  />
                                                  </xsl:element>
                                                  <xsl:element name="width"
                                                  namespace="http://www.tei-c.org/ns/1.0">
                                                  <xsl:value-of
                                                  select="substring-after($catEntry//mei:dimensions, ' x ')"
                                                  />
                                                  </xsl:element>
                                                  </xsl:element>
                                                </xsl:if>
                                            </xsl:element>
                                        </xsl:if>
                                        <xsl:if test="$catEntry//mei:condition">
                                            <xsl:call-template name="copyMeiElement">
                                                <xsl:with-param name="element"
                                                  select="$catEntry//mei:condition"/>
                                            </xsl:call-template>
                                        </xsl:if>
                                        <!-- end supportDesc -->
                                    </xsl:element>
                                    <!-- end objectDesc -->
                                </xsl:element>
                            </xsl:if>
                            <!-- get scribal hands -->
                            <xsl:if test="$catEntry//mei:handList">
                                <xsl:element name="handDesc" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:for-each select="$catEntry//mei:hand">
                                        <xsl:element name="handNote"
                                            namespace="http://www.tei-c.org/ns/1.0">
                                            <xsl:attribute name="medium" select="./@medium"/>
                                            <xsl:attribute name="scribe" select="./@resp"/>
                                            <xsl:value-of select="."/>
                                        </xsl:element>
                                    </xsl:for-each>
                                </xsl:element>
                            </xsl:if>
                        </xsl:element>
                    </xsl:if>

                    <!-- history of the source... maybe to change after upgrade to MEI 4.0 -->
                    <xsl:if test="$catEntry//mei:history">
                        <xsl:element name="history" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:for-each select="$catEntry//mei:history//mei:event">
                                <!-- Events in the source catalogue aren't that well structured as possible events in TEI, give unstructured ps instead -->
                                <xsl:element name="p" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:value-of select="concat(./mei:head, ': ', ./mei:p)"/>
                                    <xsl:if test="./mei:persName">
                                        <xsl:call-template name="copyMeiElement">
                                            <xsl:with-param name="element" select="./mei:persName"/>
                                        </xsl:call-template>
                                    </xsl:if>
                                    <xsl:if test="./mei:geogName">
                                        <xsl:call-template name="copyMeiElement">
                                            <xsl:with-param name="element" select="./mei:geogName"/>
                                        </xsl:call-template>
                                    </xsl:if>
                                    <xsl:if test="./mei:date">
                                        <xsl:call-template name="copyMeiElement">
                                            <xsl:with-param name="element" select="./mei:date"/>
                                        </xsl:call-template>
                                    </xsl:if>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:if>

                </xsl:element>
            </xsl:if>

        </xsl:if>
    </xsl:template>

    <xsl:template name="copyMeiElement">
        <xsl:param name="element"/>

        <!-- some MEI elements are nearly identical with TEI elements
        It would be wise to not catch every case by hand... -->
        <xsl:variable name="teiName">
            <xsl:choose>
                <xsl:when test="$element/local-name() = 'corpName'">
                    <xsl:value-of select="'orgName'"/>
                </xsl:when>
                <xsl:when test="$element/local-name() = 'annot'">
                    <xsl:value-of select="'note'"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$element/local-name()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:element name="{$teiName}" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:for-each select="$element/@*">
                <xsl:call-template name="copyMeiAttribute">
                    <xsl:with-param name="att" select="."/>
                </xsl:call-template>
            </xsl:for-each>
            <xsl:value-of select="$element/text()"/>
            <xsl:for-each select="$element/element()">
                <xsl:call-template name="copyMeiElement">
                    <xsl:with-param name="element" select="."/>
                </xsl:call-template>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template name="copyMeiAttribute">
        <xsl:param name="att"/>

        <!-- translate MEI attributes -->
        <xsl:choose>
            <xsl:when test="$att/name() = 'nymref'">
                <xsl:attribute name="key" select="$att"/>
                <xsl:attribute name="ref">
                    <xsl:value-of select="concat($baseUri, $att)"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="$att/name() = 'isodate'">
                <xsl:attribute name="when" select="$att"/>
            </xsl:when>
            <xsl:when test="$att/name() = 'notbefore'">
                <xsl:attribute name="notBefore" select="$att"/>
            </xsl:when>
            <xsl:when test="$att/name() = 'notafter'">
                <xsl:attribute name="notAfter" select="$att"/>
            </xsl:when>
            <xsl:when test="$att/name() = 'startdate'">
                <xsl:attribute name="from" select="$att"/>
            </xsl:when>
            <xsl:when test="$att/name() = 'enddate'">
                <xsl:attribute name="to" select="$att"/>
            </xsl:when>
            <xsl:when test="$att/name() = 'calendar'">
                <xsl:attribute name="calendar">
                    <xsl:value-of select="concat('#', $att)"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy select="$att"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- Remove line breaks. -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="tei:lb"/>

</xsl:stylesheet>
