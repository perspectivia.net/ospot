<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0">

    <xsl:output indent="yes" method="xml" encoding="UTF-16"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    
    <xsl:variable name="readings" select="//tei:note[@place = 'foot' and @type = 'a1']"/>
    <xsl:variable name="provenance" select="//tei:note[@place = 'foot' and @type = 'a2']"/>
    <xsl:variable name="annots" select="//tei:note[@place = 'foot' and @type = 'n1']"/>
    <xsl:variable name="piecenumber" select="replace(//tei:body/tei:p[1]//text()[contains(., 'piece no')],'[\],\[]','')"/>
    <xsl:variable name="pagenumber" select="//tei:body/tei:p[1]//text()[contains(., 'p. ')]"/>
    
    <xsl:variable name="piecenumbernew" select="replace($piecenumber, 'piece no.', 'Piece no.')"/>
    <xsl:variable name="pagenumbernew" select="replace($pagenumber, 'p.', 'Ms. page no.')"/>
    
    <xsl:variable name="piece" select="replace($piecenumber, '[^0-9]', '')"/>
    <xsl:variable name="page" select="translate($pagenumber, '[p. ]', '')"/>
    
    <!-- supress notes within text -->
    <xsl:template match="tei:p//tei:note[@place='foot']"/>
    <xsl:template match="tei:title/tei:idno">
        <xsl:copy>
            <xsl:value-of select="concat('TR-Iüne 204-2, ', $piecenumbernew, ', ', $pagenumbernew)"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="tei:back">
        <xsl:copy>
            <!-- create provenance app -->
            <xsl:element name="div" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="type">
                    <xsl:value-of select="'provenance'"/>
                </xsl:attribute>
                <xsl:call-template name="processApparatus">
                    <xsl:with-param name="apps" select="$provenance"/>
                    <xsl:with-param name="rdg" select="false()"/>
                </xsl:call-template>
            </xsl:element>
            <!-- create variant readings app -->
            <xsl:element name="div" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="type">
                    <xsl:value-of select="'readings'"/>
                </xsl:attribute>
                <xsl:call-template name="processApparatus">
                    <xsl:with-param name="apps" select="$readings"/>
                    <xsl:with-param name="rdg" select="true()"/>
                </xsl:call-template>
            </xsl:element>
            <!-- create annotations app -->
            <xsl:element name="div" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="type">
                    <xsl:value-of select="'annotations'"/>
                </xsl:attribute>
                <xsl:call-template name="processApparatus">
                    <xsl:with-param name="apps" select="$annots"/>
                    <xsl:with-param name="rdg" select="false()"/>
                </xsl:call-template>
            </xsl:element>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:seg">
        <xsl:variable name="segValue" select="@xml:id"/>
        <xsl:element name="anchor" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="xml:id">
                <xsl:value-of select="concat($segValue, 's')"/>
            </xsl:attribute>
        </xsl:element>
        <xsl:value-of select=".//text()"/>
        <xsl:element name="anchor" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="xml:id">
                <xsl:value-of select="concat($segValue, 'e')"/>
            </xsl:attribute>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:abbr">
        <xsl:element name="ref" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="target">
                <!--<xsl:value-of select="concat('#',@n)"/>-->
                <xsl:call-template name="updateWitIds">
                    <xsl:with-param name="witSigla" select="."/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:value-of select="text()"/>
            <!--<xsl:value-of select="replace(string-join(.//text(),''),'\s','')"/>-->
        </xsl:element>
    </xsl:template>
    
    <xsl:template name="processApparatus">
        <!-- contains the apparatus notes -->
        <xsl:param name="apps"/>
        <!-- bool: is processed as rdg -->
        <xsl:param name="rdg"/>
        
        <xsl:for-each select="$apps">
            <xsl:element name="app" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="from">
                    <xsl:variable name="valueTargetStart" select="@targetEnd"/>
                    <xsl:value-of select="concat($valueTargetStart,'s')"/>
                </xsl:attribute>
                <xsl:attribute name="to">
                    <xsl:variable name="valueTargetEnd" select="@targetEnd"/>
                    <xsl:value-of select="concat($valueTargetEnd, 'e')"/>
                </xsl:attribute>
                <xsl:if test=".//tei:mentioned/descendant-or-self::text()[matches(.,'\S')]!=''">
                    <xsl:element name="lem" namespace="http://www.tei-c.org/ns/1.0">
                        <!-- join strings if there is more than one element to replace; 
                        trouble with footnote numbers-->
                        <xsl:value-of select="replace(string-join(.//tei:mentioned/descendant-or-self::text()[matches(.,'\S')], ''), '\]', '')"/>
                        <!--<xsl:value-of select="replace(.//tei:mentioned//tei:hi, '\]', '')"/>-->
                    </xsl:element>    
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="$rdg=true() and .//tei:abbr">
                        <!-- Start SG test to create individual tei:rdg elements -->
                        <!-- Problem: Needs to distinguish between apparatus variants #1 and #2 -->
                        <!-- Hence still output with empty @wit in all cases. -->
                        <!--
                        <xsl:for-each select=".//tei:abbr">
                            <xsl:element name="rdg" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="wit">
                                    <xsl:variable name="wits">
                                        <xsl:for-each select=".//tei:abbr">
                                            <xsl:call-template name="updateWitIds">
                                                <xsl:with-param name="witSigla" select="replace(string-join(.//text(),''),'\s','')"/>
                                            </xsl:call-template>
                                        </xsl:for-each>
                                    </xsl:variable>
                                    <xsl:value-of select="normalize-space($wits)"/>
                                </xsl:attribute>
                                <xsl:apply-templates select=".//node()[preceding-sibling::tei:mentioned]"/>
                            </xsl:element>
                        </xsl:for-each>
                        -->
                        <!-- Original approach that contains all reading variants in one tei:rdg, but still with individual tei:ref. -->
                        <!-- Achtung: richtig im CTE ist: <hi>lemma<abbr>siglum</abbr></hi> -->
                        <!-- manchmal fehlerhaft <hi>lemm</hi><abbr><hi>siglum</hi></abbr> -->
                        <!-- Muss noch aufgefangen werden. -->
                        <xsl:element name="rdg" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:attribute name="wit">
                                <xsl:variable name="wits">
                                    <xsl:for-each select=".//tei:abbr">
                                        <xsl:call-template name="updateWitIds">
                                            <xsl:with-param name="witSigla" select="replace(string-join(.//text(),''),'\s','')"/>
                                        </xsl:call-template>
                                        <xsl:value-of select="' '"/>
                                    </xsl:for-each>
                                </xsl:variable>
                                <xsl:value-of select="normalize-space($wits)"/>
                            </xsl:attribute>
                            <xsl:apply-templates select=".//node()[preceding-sibling::tei:mentioned]"/>
                        </xsl:element>
                        <!-- Original approach ends here. -->
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:apply-templates select=".//node()[preceding-sibling::tei:mentioned]"/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
    
    <!-- get updated IDs for witnesses into apparati by used sigla -->
    <xsl:template name="updateWitIds">
        <xsl:param name="witSigla"/>
        <!--<xsl:value-of select="concat('#',//tei:witness[tei:idno[@type='CMO']=$witSigla]/@xml:id)"/>-->
        <xsl:value-of select="'#',//tei:witness[tei:idno[@type='CMO']=$witSigla]/@xml:id"/>
    </xsl:template>
    
    <!-- get rid of @n in first p of body -->
    <xsl:template match="tei:body/tei:p[1]/@n"/>
    
</xsl:stylesheet>
