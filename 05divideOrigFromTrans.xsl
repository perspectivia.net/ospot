<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0">

    <xsl:output indent="yes" method="xml" encoding="UTF-16"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>

    <xsl:template match="tei:body">
        <!-- get half count of p elements in body, if total count is even, otherwise false -->
        <xsl:variable name="halfedParagraphCount"
            select="
                if ((count(./tei:p) mod 2) = 0) then
                    count(./tei:p) div 2
                else
                    0"/>
        <xsl:copy>
            <!-- We have to put in the case of continued pieces original and transcription together in one div -->
            <xsl:variable name="countRtlLines"
                select="count(./tei:p[./tei:hi/@rend = 'direction:rtl;'])"/>

            <!-- 
                We split at the Turkish heading... 
                In a few cases, there is more than one Turkish heading. 
                We need to make sure, to not include Turkish headings accidentally into origPs and transPs!
                And we need to deal with "continued pieces". So we use the text direction to decide...
            -->
            <!-- Heading of transcription in Arabic alphabet -->
            <!--
            <xsl:variable name="origTitle"
                select="./tei:p[@rendition = '#rp-Title_in_Ottoman_Alphabet']"/>
            -->
            <!-- Body of transcription in Arabic alphabet -->
            <!--
            <xsl:variable name="origPs"
                select="
                    ./tei:p[not(starts-with(@rendition, '#rp-Title'))
                    and matches(string-join(descendant::tei:hi[contains(string-join(./@rend, ' '), 'direction:rtl;')]/text(), ''), '\S')]"/>
            -->
            <!-- Heading of Turkish transcription -->
            <xsl:variable name="transTitle"
                select="./tei:p[@rendition = '#rp-Title_in_Turkish_Alphabet_']"/>
            <!-- Body of Turkish transcription -->
            <xsl:variable name="transPs"
                select="
                    ./tei:p[not(starts-with(@rendition, '#rp-Title'))
                    and not(matches(string-join(descendant::tei:hi[contains(string-join(./@rend, ' '), 'direction:rtl;')]/text(), ''), '\S'))]"/>

            <!--
            <xsl:element name="div" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="type">
                    <xsl:value-of select="'transcriptionArab'"/>
                </xsl:attribute>
                <xsl:attribute name="rend">
                    <xsl:value-of select="'direction:rtl;'"/>
                </xsl:attribute>
                <xsl:apply-templates select="$origTitle"/>
                <xsl:apply-templates select="$origPs"/>
            </xsl:element>
            -->
            
            <xsl:element name="div" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="type">
                    <xsl:value-of select="'newPieceStart'"/>
                </xsl:attribute>
                
                <xsl:variable name="baseUri" select="'https://corpus-musicae-ottomanicae.de/receive/'"/>
                <xsl:variable name="pagenumber" select="//tei:body/tei:p[1]//text()[contains(., 'p. ')]"/>
                <xsl:variable name="piecenumber" select="replace(//tei:body/tei:p[1]//text()[contains(., 'piece no')],'[\],\[]','')"/>
                <xsl:variable name="piece" select="replace($piecenumber, '[^0-9]', '')"/>
                <xsl:variable name="page" select="translate($pagenumber, '[p. ]', '')"/>
                
                <xsl:element name="milestone" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="unit">
                        <xsl:value-of select="'cite'"/>
                    </xsl:attribute>
                    <xsl:attribute name="source">
                        <xsl:value-of select="'#cmo_mods_########'"/>
                    </xsl:attribute>
                </xsl:element>
                <xsl:element name="msDesc" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:element name="msIdentifier" namespace="http://www.tei-c.org/ns/1.0">
                        <xsl:element name="idno" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:value-of select="'TR-Iüne 204-2'"/>
                        </xsl:element>
                    </xsl:element>
                    <xsl:element name="msContents" namespace="http://www.tei-c.org/ns/1.0">
                        <xsl:element name="msItem" namespace="http://www.tei-c.org/ns/1.0">
                            <xsl:attribute name="n">
                                <xsl:value-of select="$piece"/>
                            </xsl:attribute>
                            <xsl:element name="locus" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="from">
                                    <xsl:value-of select="$page"/>
                                </xsl:attribute>
                                <xsl:attribute name="to">
                                    <xsl:value-of select="$page"/>
                                </xsl:attribute>
                                <xsl:value-of select="$pagenumber"/>
                            </xsl:element>
                            <xsl:element name="rubric" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:attribute name="type">
                                    <xsl:value-of select="'pieceNumber'"/>
                                </xsl:attribute>
                                <xsl:attribute name="subtype">
                                    <xsl:value-of select="'red-pencil'"/>
                                </xsl:attribute>
                                <xsl:value-of select="$piece"/>
                            </xsl:element>
                            <xsl:element name="incipit" namespace="http://www.tei-c.org/ns/1.0">
                                <xsl:element name="supplied" namespace="http://www.tei-c.org/ns/1.0">
                                    <xsl:attribute name="reason">
                                        <xsl:value-of select="'derived-from-lyrics'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="cert">
                                        <xsl:value-of select="'1'"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="resp">
                                        <xsl:value-of select="'#ND'"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="'[Enter incipit line here or delete element.]'"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:element name="div" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="type">
                    <xsl:value-of select="'blockLyricsTranscription'"/>
                </xsl:attribute>
                <xsl:attribute name="xml:lang">
                    <xsl:value-of select="'ota'"/>
                </xsl:attribute>
                <xsl:attribute name="rend">
                    <xsl:value-of select="'direction:ltr;'"/>
                </xsl:attribute>
                <xsl:apply-templates select="$transTitle"/>
                <xsl:element name="lg" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:apply-templates select="$transPs"/>
                </xsl:element>
            </xsl:element>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
