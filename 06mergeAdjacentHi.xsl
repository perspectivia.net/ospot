<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0">
    
    <xsl:output indent="yes" method="xml" encoding="UTF-16"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    
    <!-- 
        kick whitespace text nodes
        merge adjacent hi elements with identical rends 
        kick hi elements without @rend
        kick note[@place='left']
        keep everything else
    -->
    <xsl:template match="tei:div/tei:p|tei:app/tei:note|tei:app/tei:witDetail">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:for-each-group select="node()[./local-name() or matches(.,'\S')]" group-adjacent="if (local-name()) then local-name() else 'text'">
                <xsl:variable name="currentGroup" select="current-group()"/>
                <xsl:choose>
                    <xsl:when test="$currentGroup/local-name()='hi'">
                        <xsl:for-each-group select="$currentGroup[./text() and not(./note[@place='place'])]" group-adjacent="if (.[@rend]) then ./@rend else 'none'">
                            <xsl:variable name="currentRend" select="current-group()[1]/@rend"/>
                            <xsl:choose>
                                <xsl:when test="$currentRend">
                                    <xsl:element name="hi" namespace="http://www.tei-c.org/ns/1.0">
                                        <xsl:attribute name="rend">
                                            <xsl:value-of select="$currentRend"/>
                                        </xsl:attribute>
                                        <xsl:apply-templates select="current-group()/node()"/>
                                    </xsl:element>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:apply-templates select="current-group()/node()"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each-group>
                    </xsl:when>
                    <xsl:when test="$currentGroup/local-name()='note' and $currentGroup/@place='left'">
                        <!-- skip -->
                    </xsl:when>
                    <xsl:when test="$currentGroup/local-name()='p'">
                        <!-- skip -->
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="$currentGroup"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each-group>
        </xsl:copy>
        
    </xsl:template>
    
</xsl:stylesheet>