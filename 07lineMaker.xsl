<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0">
    
    <xsl:output indent="yes" method="xml" encoding="UTF-16"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    
    <xsl:variable name="origtitle" select="string-join(//tei:body/tei:div[1]/tei:p[starts-with(./@rendition, '#rp-Title')]//text(),'')"/>
    <xsl:variable name="transtitle" select="string-join(//tei:body/tei:div[2]/tei:p[starts-with(./@rendition, '#rp-Title')]//text(),'')"/>

    <xsl:template match="tei:body/tei:div/tei:p[starts-with(./@rendition, '#rp-Title')]">
        <xsl:element name="head" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="n">
                <xsl:value-of select="count(./preceding-sibling::*)+1"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*[local-name()!='n']|node()" />
            <xsl:text></xsl:text>
        </xsl:element>
    </xsl:template>
    
    <!-- transform p to l -->    
    <xsl:template match="tei:lg/tei:p[not(starts-with(./@rendition, '#rp-Title'))]">
        <xsl:element name="l" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="n">
                <xsl:value-of select="count(./preceding-sibling::*)+1"/>
            </xsl:attribute>
            <xsl:apply-templates select="@*[local-name()!='n']|node()"/>
            <xsl:text></xsl:text>
        </xsl:element>
    </xsl:template>
    
    <!-- remove all line breaks or superfluous blank spaces between l and anchor -->
    <xsl:template match="*/text()[normalize-space()]">
        <xsl:value-of select="translate(normalize-space(concat('&#x7F;',.,'&#x7F;')),'&#x7F;','')"/>
    </xsl:template>
    
    <!-- get title for header from first p in original and transliterated lyrics -->
    <xsl:template match="tei:titleStmt/tei:title/tei:title[@type = 'original']">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:value-of select="normalize-space($origtitle)"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="tei:titleStmt/tei:title/tei:title[@type = 'titleTranscription']">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <!-- Originally $transtitle, but didn't work any more at some point of time. -->
            <!-- Provisional workaround. -->
            <xsl:value-of select="normalize-space($transtitle)"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- prüfe, ob hi Attribute enthält: wenn nicht: nimm den Textknoten -->
    <!--<xsl:template match="tei:hi">
        <xsl:choose>
            <xsl:when test="not(.[@*])">
                <xsl:apply-templates select="node()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    -->
    
    <!-- if a hi within the original div only has @rend="direction:rtl;", kick it -->
    <!--
    <xsl:template match="tei:div[@type='transcriptionArab']//tei:hi">
        <xsl:choose>
            <xsl:when test="@rend='direction:rtl;'">
                <xsl:apply-templates select="node()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:div[@type='transcriptionArab']//tei:hi">
        <xsl:choose>
            <xsl:when test="@rend='font-weight:bold;direction:rtl;'">
                <xsl:apply-templates select="node()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    -->
    
</xsl:stylesheet>