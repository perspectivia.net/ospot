<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0">
    
    <xsl:output indent="yes" method="xml" encoding="UTF-16"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    
    <xsl:strip-space elements="tei:note tei:l tei:head"/>
    
    <xsl:variable name="makamList" select="document('helpers/ListMakams.xml')//tei:list/tei:item"/>
    <xsl:variable name="pieceNo" select="normalize-space(substring-after(tokenize(//tei:title/tei:idno/text(),',')[2],'no. '))"/>
    
    <!-- List item that matches the piece number -->
    <xsl:variable name="makamItem" select="$makamList[matches(@n,concat('(\D{1}|^)',$pieceNo))]"/>
   
   <!-- get author note from transcripted title -->
    <xsl:variable name="author">
        <xsl:variable name="titleTrans" select="//tei:title[@type='titleTranscription']/text()"/>
        <xsl:choose>
            <xsl:when test="contains($titleTrans,'[')">
                <!-- let's see if we have a [sic] that needs to be ignored -->
                <xsl:choose>
                    <xsl:when test="contains($titleTrans,'[sic]')">
                        <xsl:value-of select="substring-after(substring-before(replace($titleTrans, '\[sic\] ', ''), ']'),'[')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="substring-after(substring-before($titleTrans, ']'),'[')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="''"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <!-- get the transcripted title without author -->
    <xsl:variable name="transTitle">
        <xsl:variable name="titleTrans" select="//tei:title[@type='titleTranscription']/text()"/>
        <xsl:choose>
            <xsl:when test="contains($titleTrans,'[')">
                <!-- let's see if we have a [sic] that needs to be ignored -->
                <xsl:choose>
                    <xsl:when test="contains($titleTrans,'[sic]')">
                        <xsl:variable name="substring" select="substring-before(replace($titleTrans,'\[sic\]','###'), ' [')"/>
                        <xsl:value-of select="replace($substring,'###','[sic]')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="substring-before($titleTrans, ' [')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$titleTrans"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <!-- read makam from list and put it into notes of titleStmt -->
    <xsl:template match="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:note">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            
            <xsl:choose>
                <xsl:when test="@type='makamOriginal'">
                    <xsl:value-of select="$makamItem/tei:label[2]"/>
                </xsl:when>
                <xsl:when test="@type='makamTranscription'">
                    <xsl:value-of select="$makamItem/tei:label[1]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message terminate="no">
                        Something went wrong. Please fix!
                    </xsl:message>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
        <xsl:if test="not($author='')">
            <xsl:element name="author" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:value-of select="$author"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type='titleTranscription']">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:value-of select="$transTitle"/>
        </xsl:copy>
    </xsl:template>

   <!--
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="tei:body/tei:div[1]">
        <xsl:copy>
            <xsl:apply-templates select="@*|tei:lg|tei:l[@rendition='#rp-Lyricist']"/>
            <xsl:apply-templates select="tei:hi[@rendition='#rf-Lyricist']"/>
        </xsl:copy>
    </xsl:template>
    -->
    <!--
    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="tei:body/tei:div[1]">
        <xsl:copy>
            <xsl:copy-of select="following-sibling::tei:body/tei:div[1]/tei:lg/tei:l[@rendition='#rp-Lyricist']"/>
            <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>
    -->
    <!--<xsl:template match="tei:body/tei:div[1]/tei:lg/tei:l[@rendition='#rp-Lyricist']"/>-->
    
</xsl:stylesheet>