<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0">
    
    <xsl:output indent="yes" method="xml" encoding="UTF-16"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    
    <!-- all these changes do only apply to the Turkish transcription -->
    
    <xsl:variable name="semanticsForBold" select="'mainPartLyrics'"/>
    <xsl:variable name="semanticsForRegular" select="'terennüm'"/>
    <xsl:variable name="semanticsForItalic" select="'terennüm'"/>
    <xsl:variable name="segVocal" select="'vocal'"/>
    <xsl:variable name="segPerformance" select="'instruction'"/>
    
    <!-- final cleaning -->
    <!-- delete remaining attributes. -->
    <xsl:template match="@rendition"/>
    <!-- delete remaining hi. -->
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="tei:lg/tei:l/tei:hi">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:rdg/tei:hi">
        <xsl:apply-templates/>
    </xsl:template>
    <!-- delete remaining whitespaces. -->
    <xsl:template match="*/text()[normalize-space()]">
        <xsl:value-of select="normalize-space()"/>
    </xsl:template>
    <!-- delete trailing whitespace in tei:lem. -->
    <xsl:template match="tei:lem/text()[normalize-space()]">
        <xsl:value-of select="normalize-space()"/>
    </xsl:template>
    
    <xsl:strip-space elements="*"/>
    <xsl:template match="*/text()[not(normalize-space())]" />
    
    <!-- transform italics into stage directions -->
    <!--
    <xsl:template match="tei:body/tei:div[2]/tei:l/tei:hi[@rend='font-style:italic;']">
        <xsl:element name="stage" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:value-of select="* | node()"/>
        </xsl:element>
    </xsl:template>
    -->
    
    <!-- original template: add @ana to lines that are completely bold and kick bold rendition -->
    <!-- Problem: also removes <anchor> from <l> only lines when used without other SG templates. -->
    <!-- Does not seem to work any more with the other SG templates, but the <anchor> are kept. -->
    <!--
    <xsl:template match="tei:body/tei:div[2]/tei:l[not(@rendition='#rp-Lyricist')]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:choose>
                <xsl:when test="count(./*) = 1 and ./tei:hi[@rend='font-weight:bold;']">
                    <xsl:attribute name="ana">
                        <xsl:value-of select="$semanticsForBold"/>
                    </xsl:attribute>
                    <xsl:value-of select="./tei:hi[@rend='font-weight:bold;']"/>
                </xsl:when>
                <xsl:when test="not(./tei:hi[@rend='font-weight:bold;'])">
                    <xsl:attribute name="ana">
                        <xsl:value-of select="$semanticsForItalic"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="node()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="node()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>
    -->
    <!-- Other SG idea... -->
    <!--
    <xsl:template match="tei:body/tei:div[2]/tei:l[not(@rendition='#rp-Lyricist')]">
            <xsl:apply-templates select="@*"/>
                <xsl:for-each select="./tei:hi[@rend='font-weight:bold;']">
                    <xsl:attribute name="ana">
                        <xsl:value-of select="$semanticsForBold"/>
                    </xsl:attribute>
                    <xsl:value-of select="./tei:hi[@rend='font-weight:bold;']"/>
                </xsl:for-each>
    </xsl:template>
    -->

    <!-- template SG: Template to replace [] by tei:supplied. -->
    <xsl:template match="tei:body/tei:div[2]/tei:lg/tei:l/descendant-or-self::text()">
        <xsl:analyze-string select="." regex="\[.*\]">
            <xsl:matching-substring>
                <xsl:element name="supplied" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="reason">
                        <xsl:value-of select="'omitted-in-original'"/>
                    </xsl:attribute>
                    <xsl:attribute name="cert">
                        <xsl:value-of select="'1'"/>
                    </xsl:attribute>
                    <xsl:attribute name="resp">
                        <xsl:value-of select="'#ND'"/>
                    </xsl:attribute>
                    <xsl:value-of select="translate(., '[]', '')"/>
                </xsl:element>
            </xsl:matching-substring>
            <xsl:non-matching-substring>
                <xsl:value-of select="."/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:template>
   
    <!-- template SG: transform bold parts within lines into <seg type="form" ana="mainPartLyrics" -->
    <xsl:template match="tei:body/tei:div[2]/tei:lg/tei:l/tei:hi[@rend='font-weight:bold;']">
        <xsl:element name="seg" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="type">
                <xsl:value-of select="$segVocal"/>
            </xsl:attribute>
            <xsl:attribute name="ana">
                <xsl:value-of select="$semanticsForBold"/>
            </xsl:attribute>
            <xsl:apply-templates select="node()"></xsl:apply-templates>
            <xsl:text></xsl:text>
        </xsl:element>
    </xsl:template>
    
    <!-- template SG: transform regular parts within lines into <seg type="form" ana="terennüm" -->
    <xsl:template match="tei:body/tei:div[2]/tei:lg/tei:l/tei:hi[@rendition='#rf-Line_blue']">
        <xsl:element name="seg" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="type">
                <xsl:value-of select="$segVocal"/>
            </xsl:attribute>
            <xsl:attribute name="ana">
                <xsl:value-of select="$semanticsForRegular"/>
            </xsl:attribute>
            <xsl:apply-templates select="node()"></xsl:apply-templates>
            <xsl:text></xsl:text>
        </xsl:element>
    </xsl:template>

    <!-- template SG: transform italic parts within lines into <seg type="information" ana="terennüm" and <stage> -->
    <xsl:template match="tei:body/tei:div[2]/tei:lg/tei:l/tei:hi[@rend='font-style:italic;']">
        <xsl:element name="stage" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="type"/>
            <xsl:attribute name="ana"/>
            <xsl:element name="seg" namespace="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="type">
                <xsl:value-of select="$segPerformance"/>
            </xsl:attribute>
            <xsl:attribute name="ana">
                <xsl:value-of select="$semanticsForItalic"/>
            </xsl:attribute>
                <xsl:apply-templates select="node()"></xsl:apply-templates>
                <xsl:text></xsl:text>
            </xsl:element>
        </xsl:element>
    </xsl:template>
   
    <!-- Move lyricist line one node up to div, following the last tei:lg. -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="tei:lg">
        <xsl:copy>
            <xsl:attribute name="n">
                <xsl:value-of select="'1'"/>
            </xsl:attribute>
            <xsl:attribute name="decls">
                <xsl:value-of select="'[#vezin@xml:id]'"/>
            </xsl:attribute>
            <xsl:attribute name="rhyme"></xsl:attribute>
            <xsl:apply-templates select="child::node()[not(self::tei:l[@rendition='#rp-Lyricist'])]"/>
        </xsl:copy>
        <xsl:apply-templates select="tei:l[@rendition='#rp-Lyricist']"/>
    </xsl:template>

    <!-- template SG: transform regular parts within lines into <note type="lyricist" -->
    <xsl:template match="tei:l[(@rendition='#rp-Lyricist')]">
        <!-- Trying to remove the square brackets also removes the tei:anchor elements. -->
        <!--
        <xsl:analyze-string select="." regex="\[.*\]">
            <xsl:matching-substring>
                <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="type">
                        <xsl:value-of select="'lyricist'"/>
                    </xsl:attribute>
                        <xsl:element name="supplied" namespace="http://www.tei-c.org/ns/1.0">
                        <xsl:attribute name="reason">
                            <xsl:value-of select="'provided-by-editor'"/>
                        </xsl:attribute>
                        <xsl:attribute name="cert">
                            <xsl:value-of select="'1'"/>
                        </xsl:attribute>
                        <xsl:attribute name="resp">
                            <xsl:value-of select="'#ND'"/>
                        </xsl:attribute>
                        <xsl:value-of select="translate(., '[]', '')"/>
                    </xsl:element>
                </xsl:element>
            </xsl:matching-substring>
        </xsl:analyze-string>
        -->
        <xsl:element name="note" namespace="http://www.tei-c.org/ns/1.0">
                <xsl:attribute name="type">
                    <xsl:value-of select="'lyricist'"/>
                </xsl:attribute>
                <xsl:element name="supplied" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:attribute name="reason">
                        <xsl:value-of select="'provided-by-editor'"/>
                    </xsl:attribute>
                    <xsl:attribute name="cert">
                        <xsl:value-of select="''"/>
                    </xsl:attribute>
                    <xsl:attribute name="resp">
                        <xsl:value-of select="'#ND'"/>
                    </xsl:attribute>
                <xsl:apply-templates select="node()"></xsl:apply-templates>
                <xsl:text></xsl:text>
                </xsl:element>
            </xsl:element>
    </xsl:template>
    
    
    <!-- template SG: transform regular text into ana="terennüm" -->
    <!--
    <xsl:template match="tei:body/tei:div[2]/tei:l[not(@rendition='#rp-Lyricist')]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:choose>
                <xsl:when test="not(./tei:hi[@rend='font-weight:bold;'])">
                    <xsl:attribute name="ana">
                        <xsl:value-of select="$semanticsForItalic"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="node()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="node()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>
    -->
    
    <!-- template SG: and do so for the lyricits with ana="lyricist" -->
    <!--
    <xsl:template match="tei:body/tei:div[2]/tei:l[(@rendition='#rp-Lyricist')]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:choose>
                <xsl:when test="not(./tei:hi[@rend='font-weight:bold;'])">
                    <xsl:attribute name="ana">
                        <xsl:value-of select="$semanticsForLyricist"/>
                    </xsl:attribute>
                    <xsl:apply-templates select="node()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="node()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>
    -->
    
</xsl:stylesheet>