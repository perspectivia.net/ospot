<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="3.0">
    
    <xsl:output method="text" encoding="UTF-8"/>
    <xsl:mode on-no-match="shallow-skip"/>
    
    <xsl:template match="/">
        
        <xsl:variable name="rends" select=".//@rend"/>
        
        <xsl:variable name="rend_item_list" select="string-join($rends,';')"/>
                
        <xsl:variable name="tokenized_rendList" select="tokenize($rend_item_list,';')"/>
        <xsl:variable name="distenct_rends" select="distinct-values($tokenized_rendList)"/>

        
        <xsl:for-each select="$distenct_rends">
            <xsl:sort select="." order="ascending"></xsl:sort>
            <xsl:copy-of select="."/>
            <xsl:text>&#xa;</xsl:text>
        </xsl:for-each>
    </xsl:template>
            
    
</xsl:stylesheet>