<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0" version="3.0">
    
    <xsl:output indent="yes" method="xml"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    <!-- anpassen; globale Variable mit einzelnen Templates befüllen -> als Kind von; when otherwise Konstrukt -->
    <xsl:template match="@rend">
        <xsl:variable name="content" select="."/>
        <xsl:variable name="output" select="$content"/>

        <!-- tokenize rend values -->
        <xsl:variable name="values_seq" select="tokenize($output, ';')"/>
        <xsl:variable name="get_index" select="index-of($values_seq, $output)"/>
        
        
        <xsl:attribute name="rend">
            <xsl:value-of select="string-join($get_index, ';')"/>
        </xsl:attribute>



    </xsl:template>

</xsl:stylesheet>
