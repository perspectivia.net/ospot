<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    version="3.0">
    
    <xsl:output indent="yes" method="xml"/>
    <!-- copy every node in file -->
    <xsl:mode on-no-match="shallow-copy"/>
    
    <!-- 
        Damit kriegt man alle body/p, die einen Textknoten mit 'piece no' als descendant haben:
        //text()[contains(.,'piece no')]/ancestor::p[parent::body]
    -->
    
    
    <xsl:template match="tei:body">
        <!-- get a variable with all the p that are children of body -->
        <xsl:variable name="pieces">
            <!-- group them according to "piece no" in descendant hi elements -->
            <xsl:for-each-group select="*" group-starting-with="tei:p[descendant::tei:hi[contains(.,'piece no')]]">
                <!-- store piece no in a variable -->
                <xsl:variable name="pieceNo" select="replace(.//tei:hi[contains(., 'piece no.')],'\W','')"/>
                <!-- put an @n attribute with the piece no to each p in the current group -->
                <xsl:for-each select="current-group()">
                    <xsl:copy>
                        <xsl:attribute name="n" select="$pieceNo"/>
                        <xsl:apply-templates select="@* | node()"/>
                    </xsl:copy>
                </xsl:for-each>
            </xsl:for-each-group>
        </xsl:variable>
        
        <xsl:copy>
            <!-- now, we write the content -->
            <!-- group all the ps in our variable according to the @n we added -->
            <xsl:for-each-group select="$pieces/tei:p" group-adjacent="@n">
                <!-- put the groups into divs -->
                <xsl:element name="div" namespace="http://www.tei-c.org/ns/1.0">
                    <xsl:copy-of select="current-group()"/>
                </xsl:element>
            </xsl:for-each-group>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>